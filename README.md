# CrossGen #

Very simple tool for generating crosswords. *Almost* course work.

### Features ###

1. Supports many of languages if dictionary contains lines like
```
WORD - DESCRIPTION
```
2. Supports any grid that have format like
```
++++
+--+
+--+
++++
```

### Dependecies ###
Only wxWidgets.

### To-Do list ###

* [ ] Progress indication
* [ ] Customiztion
* [ ] Refactoring
* [ ] Optimizations of generating crosswords